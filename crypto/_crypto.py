#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from nacl.hash import sha512
from nacl.hashlib import blake2b
from nacl.public import Box,SealedBox
from nacl.secret import SecretBox
from nacl.signing import SigningKey
from nacl.utils import random as randombytes

from ._helper import _str2bytes,_bytes2str,_hex2bytes,_bytes2hex,_output
from ._helper import _as_private_key,_as_public_key
from ._helper import _as_signing_key,_as_verify_key

def generate_prikey(as_hex=False):
    # generate prikey
    return _output(SigningKey.generate(),
                   as_hex=as_hex)

def generate_pubkey(key,as_hex=False):
    # generate pubkey from key
    return _output(_as_signing_key(key).verify_key,
                   as_hex=as_hex)

def generate_seckey(as_hex=False):
    # generate skey
    return _output(randombytes(SecretBox.KEY_SIZE),
                   as_hex=as_hex)

def passphrase(s):
    # convert any length string to skey
    return blake2b(sha512(s.encode('utf8')),
        digest_size=SecretBox.KEY_SIZE).digest()

def keygen(as_hex=False):
    # generate prikey and pubkey pair
    key=generate_prikey(as_hex=as_hex)
    return key,generate_pubkey(key,as_hex=as_hex)

def sign(data,key):
    # sign data with key, return signature
    return _as_signing_key(key).sign(data).signature.hex()

def verify(data,signature,key):
    # verify data with signature using key, return True if successfully else False
    try:
        _as_verify_key(key).verify(data,bytes.fromhex(signature))
    except:
        return False
    else:
        return True

def encrypt0(data,skey):
    # symmetric encrypt data with skey, return encrypted data
    return SecretBox(_hex2bytes(skey)).encrypt(data)

def decrypt0(data,skey):
    # symmetric decrypt data using skey, return decrypted data
    return SecretBox(_hex2bytes(skey)).decrypt(data)

def encrypt1(data,pubkey):
    # asymmetric encrypt data with pubkey, return encrypted data
    return SealedBox(_as_public_key(pubkey)).encrypt(data)

def decrypt1(data,prikey):
    # asymmetric decrypt data using prikey, return decrypted data
    return SealedBox(_as_private_key(prikey)).decrypt(data)

def encrypt2(data,prikey,pubkey):
    # asymmetric encrypt data with prikey and pubkey, return encrypted data
    return Box(_as_private_key(prikey),
               _as_public_key(pubkey)).encrypt(data)

def decrypt2(data,prikey,pubkey):
    # asymmetric decrypt data using prikey and pubkey, return decrypted data
    return Box(_as_private_key(prikey),
               _as_public_key(pubkey)).decrypt(data)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
