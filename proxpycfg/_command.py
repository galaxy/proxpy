#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from functools import partial
from getpass import getpass
from string import ascii_lowercase,ascii_uppercase,digits,punctuation

from ._module_dir import MODULE_DIR
from ._saveload import nonedict,initconf,_saveconf,_loadconf,PasswordRequired

ALLOWED_PORT_RANGE=range(1024,65536)

saveconf=partial(_saveconf,module_dir=MODULE_DIR)
loadconf=partial(_loadconf,module_dir=MODULE_DIR)

def _prompt_loadconf(params):
    try:
        return loadconf(params.side,passwd=params.loadpasswd)
    except PasswordRequired:
        params.loadpasswd=getpass(prompt='Load with password: ')
        return loadconf(params.side,passwd=params.loadpasswd)

def _pass_saveconf(params,conf):
    if not params.nopasswd:
        if params.chpasswd:
            if params.passwd is None:
                params.passwd=getpass(prompt='Save with password: ')
            if s:=_precheck_password(params.passwd):
                return print(s) or 1
        else:
            params.passwd=params.loadpasswd
    saveconf(params.side,conf,passwd=params.passwd)

def prompt_loadconf(passwd=None):
    try:
        return loadconf('client',passwd=passwd)
    except PasswordRequired:
        passwd=getpass(prompt='Load with password: ')
        return loadconf('client',passwd=passwd)

def _precheck_password(password):
    passet=set(password)
    if len(passet)<8:
        return 'requires at least 8 different chars'
    if sum((bool(passet&set(ascii_lowercase)),
            bool(passet&set(ascii_uppercase)),
            bool(passet&set(digits)),
            bool(passet&set(punctuation)),
            ))<3:
        return 'requires at least 3 types of uppers, lowers, digits and symbols.'
    return None

def create_conf(params):
    conf=initconf(serverside=params.side=='server')
    params.chpasswd=True
    return _pass_saveconf(params,conf)

def setpath_conf(params):
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    conf['path']=params.path
    return _pass_saveconf(params,conf)

def setdns_conf(params):
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    import dohjson
    if not (p:=dohjson.MODULE_DIR/params.cacert).exists():
        print('WW: {p} not exists.')
    conf['doh']['ip']=params.ip
    conf['doh']['port']=params.port
    conf['doh']['name']=params.name
    conf['doh']['path']=params.path
    conf['doh']['cacert']=params.cacert
    return _pass_saveconf(params,conf)

def sethttp_conf(params):
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    if not (p:=params.rootdir/params.cacert).exists():
        print('WW: {p} not exists.')
    conf['cacert']=params.cacert
    conf['cache']=params.cache
    return _pass_saveconf(params,conf)

def setcert_conf(params):
    if not params.ca_name:
        return print('ca name is empty.') or 1
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    conf['proxy']['keyalg']=params.keyalg
    conf['proxy']['curve']=params.curve
    conf['proxy']['md']=params.md
    conf['proxy']['ca_name']=params.ca_name
    conf['proxy']['ca_expire']=params.ca_expire
    conf['proxy']['site_expire']=params.site_expire
    return _pass_saveconf(params,conf)

def setproxy_conf(params):
    if not params.proxy_name:
        return print('proxy name is empty.') or 1
    if params.proxy_port not in ALLOWED_PORT_RANGE:
        return print(f'port {params.proxy_port} not in range: '
                     f'{ALLOWED_PORT_RANGE[0]} - {ALLOWED_PORT_RANGE[-1]}') or 1
    from mkcert import isipaddr
    if not isipaddr(params.proxy_ip):
        return print(f'invalid ip: {params.proxy_ip}') or 1
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    for name,proxy in conf['proxy']['servers'].items():
        x,ip,port=proxy
        if (params.proxy_ip,params.proxy_port)==(ip,port):
            return print(f'{ip}:{port} already used by {name}') or 1
    conf['proxy']['servers'][params.proxy_name]=[
        params.proxy_type,params.proxy_ip,params.proxy_port,
    ]
    return _pass_saveconf(params,conf)

def delproxy_conf(params):
    if not params.proxy_name:
        return print('proxy name is empty.') or 1
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    conf['proxy']['servers'][params.proxy_name]=None
    return _pass_saveconf(params,conf)

def remote_conf(params):
    if (conf:=_prompt_loadconf(params)) is None:
        return 1
    from ._remote import setremote
    if (s:=setremote(conf,params)) is None:
        return print(f'failed to set remote {params.remote_name}') or 1
    server_pubkey,signature=s
    conf['remote']['ip']=params.remote_ip or params.remote_name
    conf['remote']['name']=params.remote_name
    conf['remote']['port']=params.remote_port
    conf['remote']['path']=params.remote_path
    conf['remote']['https']=not params.remote_nohttps
    conf['remote']['sni']=not params.remote_nosni
    conf['remote']['cache']=params.cache
    conf['remote']['pubkey']=server_pubkey
    conf['remote']['signature']=signature
    return _pass_saveconf(params,conf)

def show_conf(params):
    if (conf:=_prompt_loadconf(params)) is None:
        return 1

    from . import _fmt as fmt
    import croxy
    import http0
    import dohjson
    print(f'{params.side} side config:')
    print()
    print(fmt.HTTP_FMT.format(c=conf,d=http0.MODULE_DIR,r=params.rootdir))
    print(fmt.DOH_FMT.format(c=conf,d=dohjson.MODULE_DIR))

    if params.side=='server':
        return print(fmt.SERV_FMT.format(c=conf))
    print(fmt.CERT_FMT.format(
        c=conf,crt=(params.rootdir/conf['proxy']['ca_name']).with_suffix('.crt')
    ))
    print(fmt.REMOTE_FMT.format(c=conf,d=croxy.MODULE_DIR))
    for name,server in conf['proxy']['servers'].items():
        print(fmt.PROXY_FMT.format(name,server))

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
