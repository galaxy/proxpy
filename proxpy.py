#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from collections import OrderedDict,deque
from http import HTTPStatus
from http.client import HTTP_PORT,HTTPS_PORT
from http.server import BaseHTTPRequestHandler
from json import dumps,loads
from pathlib import Path
from os.path import dirname,isfile,join
from ssl import SSLContext,PROTOCOL_TLS,create_default_context
from socketserver import ThreadingTCPServer,StreamRequestHandler
from tempfile import TemporaryDirectory
from threading import Thread,Lock
from time import gmtime,strftime,time
from urllib.parse import urlparse as _urlparse

from idpool import IDPOOL

from mkcert import mkcert,mkparams,isipaddr
from dohjson import DNSClient

from croxy import CryptoConnection as _CryptoConnection
from croxy import CacheCryptoConnection as _CacheCryptoConnection
from http0 import HTTPConnection as _HTTPConnection
from http0 import LocalCacheConnection as _LocalCacheConnection

from proxpycfg import prompt_loadconf,nonedict

try:
    from certfilter import certfilter
except:
    certfilter=None

# maximal line length when calling readline().
_MAXLINE=65536

class ABC:
    def __init__(self,**kwds):
        for k,v in kwds.items():
            setattr(self,k,v)

PROXYENV=ABC(
    ROOT=Path(__file__).resolve().parent,
    CACERT=None,
    DATADIR=None,
    SERVER_NAME='Proxpy',
    TLSCTX={},
    LOCK=Lock(),
    IDP=IDPOOL()
)

MKCERT_ENV=nonedict()
REMOTE_ENV=nonedict()

def now():
    return strftime('%FT%TZ',gmtime(time()))

def log(msg,/,uid=None):
    if uid:
        return print(f'{now()}({uid}) {msg}')
    return print(f'{now()} {msg}')

def new_tls_context():
    ctx=create_default_context(cafile=PROXYENV.ROOT/PROXYENV.CACERT)
    ctx.set_alpn_protocols(['http/1.1'])
    return ctx

class DirectConnection(_HTTPConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        if scheme=='https':
            kwds['context']=new_tls_context()
        super().__init__(hostname,port=port,certfilter=certfilter,**kwds)

class CachedConnection(_LocalCacheConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        if scheme=='https':
            kwds['context']=new_tls_context()
        super().__init__(hostname,port=port,cachedir=PROXYENV.DATADIR,
                         certfilter=certfilter,logger=log,**kwds)

class NoSNIConnection(DirectConnection):
    def __init__(self,*args,**kwds):
        super().__init__(*args,sni=False,**kwds)

class CryptoConnection(_CryptoConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        ctx=None
        if REMOTE_ENV['https']:
            ctx=new_tls_context()
        if scheme=='https':
            kwds['context']=new_tls_context()
        super().__init__(hostname,port=port,certfilter=certfilter,**kwds)
        self.setremote(
            REMOTE_ENV['ip'],port=REMOTE_ENV['port'],path=REMOTE_ENV['path'],
            hostname=REMOTE_ENV['name'],tlsname=REMOTE_ENV['name'],
            context=ctx,sni=REMOTE_ENV['sni'],pubkey=REMOTE_ENV['pubkey'],
            client_prikey=REMOTE_ENV['prikey'],signature=REMOTE_ENV['signature'])

class CacheCryptoConnection(_CacheCryptoConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        ctx=None
        if REMOTE_ENV['https']:
            ctx=new_tls_context()
        if scheme=='https':
            kwds['context']=new_tls_context()
        kwds['cachedir']=REMOTE_ENV['cache']
        super().__init__(hostname,port=port,certfilter=certfilter,
                         logger=log,**kwds)
        self.setremote(
            REMOTE_ENV['ip'],port=REMOTE_ENV['port'],path=REMOTE_ENV['path'],
            hostname=REMOTE_ENV['name'],tlsname=REMOTE_ENV['name'],
            context=ctx,sni=REMOTE_ENV['sni'],pubkey=REMOTE_ENV['pubkey'],
            client_prikey=REMOTE_ENV['prikey'],signature=REMOTE_ENV['signature'])

BACKEND={
    'direct':DirectConnection,
    'cached':CachedConnection,
    'nosni':NoSNIConnection,
    'crypto':CryptoConnection,
    'ccrypto':CacheCryptoConnection,
}

def urlparse(url):
    return _urlparse(url,allow_fragments=False)

def certpair(serv):
    if not serv:
        raise ValueError('server name is empty.')
    params=mkparams(update={
        'serv':serv,
        'dnslist':[serv],
        **MKCERT_ENV})
    return mkcert(params)

def wrap_tls_server(s,host,uid=None):
    with PROXYENV.LOCK:
        if host not in PROXYENV.TLSCTX:
            crt,key=certpair(host)
            PROXYENV.TLSCTX[host]=SSLContext(protocol=PROTOCOL_TLS)
            PROXYENV.TLSCTX[host].load_cert_chain(certfile=crt,keyfile=key)
            log(f'mkcert: new proxy cert for {host}',uid)
    try:
        return PROXYENV.TLSCTX[host].wrap_socket(s,server_side=True)
    except Exception as e:
        return log(f'mkcert: ssl connect failed: {host} {e}',uid)

class ServerThread(Thread):
    def __init__(self,*args,**kwds):
        super().__init__(*args,**kwds)
        self.daemon=True
        self.start()

class ProxyHandler(BaseHTTPRequestHandler):
    protocol_version='HTTP/1.1'
    supported_method=('CONNECT','GET','POST','HEAD')
    default_scheme_port={'http':HTTP_PORT,'https':HTTPS_PORT}

    def __init__(self,*args,**kwds):
        self.lock=Lock()
        self.destsrv=None
        self.parsed_url=urlparse('http:/')
        self.uid=PROXYENV.IDP.newid()
        self.handle_count=0
        self.handle_noreq_count=0
        return super().__init__(*args,**kwds)

    def handle(self):
        log(f'{self.server.name}: new handler',self.uid)
        self.upstream=None
        super().handle()
        if self.upstream is not None:
            upstream=self.upstream
            self.upstream=None
            upstream.close()
        hostname=self.parsed_url.hostname
        url=f': {self.parsed_url.scheme}:{hostname}' if hostname else ''
        count=f'(handle {self.handle_count}'
        if self.handle_noreq_count:
            count=f'{count} with {self.handle_noreq_count} no request)'
        else:
            count=f'{count})'
        log(f'{self.server.name}: close handler{url} {count}',
            self.uid)
        PROXYENV.IDP.delid(self.uid)

    def handle_one_request(self):
        with self.lock:
            url=''
            if self.parsed_url.hostname:
                url=f': {self.parsed_url.scheme}:{self.parsed_url.hostname}'
            log(f'{self.server.name}: handler start{url}',self.uid)
            super().handle_one_request()
            if self.parsed_url.hostname:
                url=f'{self.parsed_url.scheme}:{self.parsed_url.hostname}'
                self.handle_count+=1
            else:
                url='no request'
                self.handle_noreq_count+=1
            log(f'{self.server.name}: handler end: {url}',self.uid)

    def version_string(self):
        return PROXYENV.SERVER_NAME

    def log_request(self,code='-',size='-'):
        if self.command=='CONNECT':return
        return #super().log_request(code=code,size=size)

    def receive(self,length,blocksize=1024):
        while length>0 and not self.rfile.closed:
            yield (b:=self.rfile.read(min(length,blocksize)))
            length-=len(b)

    def receive_chunked(self):
        while size:=self._next_chunk_size():
            yield self.rfile.read(size)
            if self.rfile.readline().rstrip(b'\r\n'):
                raise Exception('chunk data error')

    def _next_chunk_size(self):
        sizeline=self.rfile.readline(_MAXLINE+1)
        if len(sizeline)>_MAXLINE:
            raise Exception('chunk size too long')
        extensions=sizeline.rstrip(b'\r\n').split(b';')
        try:
            size=int(extensions.pop(0),base=16)
        except ValueError:
            self.close()
            raise Exception('unknown byte in chunk size')
        return size

    def do_before(self,post=False):
        # receive/parse the request of client
        if post:
            if self.headers.get('transfer-encoding','').lower()=='chunked':
                receive_iter=self.receive_chunked()
            elif 'content-length' in self.headers:
                receive_iter=self.receive(int(self.headers.get('content-length')))
            else:
                self.send_error(HTTPStatus.LENGTH_REQUIRED,
                                'POST without content-length is not allowed')
                self.upstream=None
                return
        else:
            receive_iter=None
        if 'proxy-connection' in self.headers:
            del self.headers['proxy-connection']
        urlres=urlparse(self.path)
        port=urlres.port or self.parsed_url.port
        netloc=urlres.hostname or self.parsed_url.hostname
        if port and port!=self.default_scheme_port[self.parsed_url.scheme]:
            netloc=f'{netloc}:{port}'
        if self.parsed_url.scheme!='https':
            self.parsed_url=self.parsed_url._replace(netloc=netloc)
        self.parsed_url=self.parsed_url._replace(
            netloc=netloc,path=urlres.path,
            params=urlres.params,query=urlres.query)

        # create upstream and request
        method=self.command
        scheme=self.parsed_url.scheme
        hostname=self.parsed_url.hostname
        port=self.parsed_url.port
        path=self.parsed_url.path
        query=self.parsed_url.query
        headers=OrderedDict((k.lower(),v) for k,v in self.headers.items())
        if self.upstream is None:
            log(f'{self.server.name}: new upstream ({self.server.backend}): '
                f'{scheme}:{hostname}',self.uid)
            self.upstream=BACKEND[self.server.backend](
                hostname,port=port,scheme=scheme,uid=self.uid,
                dnsclient=self.server.dnsclient)
        log(f'{self.server.name}: request: '
            f'{method} {scheme}:{hostname}{path}',self.uid)
        try:
            self.upstream.request(
                method,f'{path}?{query}' if query else path,
                body=receive_iter,
                headers=headers)
        except Exception as e:
            log(f'{self.server.name}: request failed: '
                f'{method} {scheme}:{hostname}{path} ({e})',self.uid)
            self.close_connection=True
            return
        else:
            return self.upstream

    def do_after(self):
        scheme=self.parsed_url.scheme
        hostname=self.parsed_url.hostname
        path=self.parsed_url.path
        internal_err=False
        # receive/parse the response of upstream and send response to client
        status=self.upstream.status
        response_headers=self.upstream.response_headers.copy()
        is_chunked=response_headers.get('transfer-encoding','').lower()=='chunked'
        if status>599:
            status-=100
            internal_err=True
        else:
            log(f'{self.server.name}: response ({status}): '
                f'{self.command} {scheme}:{hostname}{path}',self.uid)
        self.start_response(status,response_headers)
        if status>399:
            response_headers['connection']='close'
        if status>299:
            location=response_headers.get('location',None)
            if location is not None:
                # close if redirect to another site
                other=urlparse(location)
                if other.hostname and other.hostname!=hostname:
                    response_headers['connection']='close'
                if other.scheme and other.scheme!=scheme:
                    response_headers['connection']='close'
                if other.port and other.port!=port:
                    response_headers['connection']='close'
        if response_headers.get('connection','').lower()=='close':
            self.close_connection=True
        try:
            if internal_err:
                data=b''.join(self.upstream.response_iter())
                msg=data.decode('utf8')
                if is_chunked:
                    self.send_chunk(data)
                    self.send_chunk()
                else:
                    self.send_data(data)
                log(f'{self.server.name}: response ({status+100} {msg}): '
                    f'{scheme}:{hostname}{path}',self.uid)
            elif is_chunked:
                for block in self.upstream.response_iter():
                    self.send_chunk(block)
                self.send_chunk()
            else:
                for block in self.upstream.response_iter():
                    self.send_data(block)
        except BrokenPipeError:
            # client closed.
            self.close_connection=True
        except Exception as e:
            self.close_connection=True
            log(f'{self.server.name}: response interrupt: '
                f'{self.command} {scheme}:{hostname}{path} ({e})',self.uid)

    def start_response(self,status,headers):
        self.send_response(status)
        for k,v in headers.items():
            self.send_header(k,v)
        self.end_headers()

    def send_data(self,data):
        self.wfile.write(data)
        self.wfile.flush()

    def send_chunk(self,data=b''):
        self.send_data(f'{len(data):x}'.encode('utf8'))
        self.send_data(b'\r\n')
        self.send_data(data)
        self.send_data(b'\r\n')

    def do_CONNECT(self):
        self.parsed_url=self.parsed_url._replace(scheme='https',netloc=self.path)
        host=self.parsed_url.hostname
        log(f'{self.server.name}: client connect: '
            f'{self.parsed_url.scheme}:{host}',self.uid)
        self.destsrv=host
        self.start_response(HTTPStatus.OK,{})
        self.request=wrap_tls_server(self.request,host,uid=self.uid)
        if self.request is None:
            self.close_connection=True
            return
        self.setup()
        conntype=self.headers.get('proxy-connection','close').lower()
        self.close_connection=conntype!='keep-alive'

    def do_main(self,post=False):
        if self.do_before(post=post) is None:
            return
        if self.upstream is None:return
        self.do_after()

    def do_GET(self):
        return self.do_main()

    def do_POST(self):
        return self.do_main(post=True)

class ProxyServer(ThreadingTCPServer):
    def __init__(self,servaddr,handler,dnsclient,backend,name=None):
        super().__init__(servaddr,handler)
        self.dnsclient=dnsclient
        self.backend=backend
        self.name=name

def newproxy(name=None,ip=None,port=None,backend=None,dnsclient=None,pool={}):
    PROXYENV.LOCK.acquire()
    if name in pool:
        PROXYENV.LOCK.release()
        raise ValueError(f'server name duplicate: {name}')
    with ProxyServer((ip,port),ProxyHandler,
                     dnsclient,backend,name=name) as server:
        try:
            pool[name]=server
            log(f'run {name} on {ip}:{port}')
            PROXYENV.LOCK.release()
            server.serve_forever()
        except KeyboardInterrupt:
            server.shutdown()
    log(f'{name} end')

def main():
    parser=ArgumentParser(prog='cli_config')
    parser.add_argument(
        '-P',dest='passwd',action='store',metavar='PASSWORD',
        help='password to load config file.')
    if (conf:=prompt_loadconf(parser.parse_args().passwd)) is None:
        return 1
    if not conf['proxy']['servers']:
        return log('no server exists.') or 1
    serverpool={}
    threadpool=[]
    REMOTE_ENV['prikey']=conf['prikey']
    REMOTE_ENV.update(conf['remote'])
    MKCERT_ENV.update(
        ca=conf['proxy']['ca_name'],
        cadir=PROXYENV.ROOT,
        keyalg=conf['proxy']['keyalg'],
        curve=conf['proxy']['curve'],
        md=conf['proxy']['md'],
        cadays=conf['proxy']['ca_expire'],
        servdays=conf['proxy']['site_expire'],
        overwrite_ca=False,
        overwrite_serv=False,
    )
    PROXYENV.CACERT=conf['cacert']
    PROXYENV.DATADIR=conf['cache']
    with TemporaryDirectory(dir=PROXYENV.ROOT,prefix='site-certs-') as _tmpdir:
        with DNSClient(dns_ip=conf['doh']['ip'],
                       dns_port=conf['doh']['port'],
                       dns_name=conf['doh']['name'],
                       dns_path=conf['doh']['path'],
                       cacert=conf['doh']['cacert']) as dns:
            tmpdir=Path(_tmpdir).resolve()
            MKCERT_ENV['servdir']=tmpdir
            for name,serv in conf['proxy']['servers'].items():
                backend_type,ip,port=serv
                threadpool.append(ServerThread(
                    target=newproxy,kwargs={
                        'name':name,'ip':ip,'port':port,
                        'backend':backend_type,
                        'dnsclient':dns,'pool':serverpool,
                    }))
            for thread in threadpool:
                try:
                    thread.join()
                except KeyboardInterrupt:
                    for server in serverpool.values():
                        server.shutdown()

if __name__=='__main__':
    exit(main())

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
